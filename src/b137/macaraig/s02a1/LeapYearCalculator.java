package b137.macaraig.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args) {
        System.out.println("Leap Year Calculator \n");

        Scanner appScanner = new Scanner(System.in);
//
//        System.out.println("What is your first name?\n");
//        String firstName = appScanner.nextLine();
//
//        System.out.println("Hello, " + firstName + " !\n");

        //Activity: Create a program that checks if a year is a leap year or not.

        System.out.println("Please enter a year to check.\n");

        int year = appScanner.nextInt();

        //String year = appScanner.nextLine();

        if ((year == 2000) || (year == 2004) ||(year == 2012)) {
            System.out.println("It is a leap year");
        } else {
            System.out.println("It is not a leap year");
        }


    }
}